var mongoose = require('mongoose');

var UserRepository = require('../Repository/UserRepository');
var _user= new UserRepository();
class UserService {
    constructor() {

    }

createUser(res, user) {
    try {
      
       _user.create(res,user)
        
    } catch (error) {
    }

}

getUsers (res)  {
    try {
      return  _user.get(res);
    } catch (error) {
    }
}



getUsersById (req, res) {
    return _user.getByID(req, res);
}



deleteUser(res, id) {
   
    return _user.delete(res, id);
}
updateUser(req, res){
    return _user.update(req,res);
}
}
module.exports = UserService;