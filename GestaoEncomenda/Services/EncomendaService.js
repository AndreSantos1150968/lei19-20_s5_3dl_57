var mongoose = require('mongoose');

var EncomendaRepository = require('../Repository/EncomendaRepository');
var _encomenda= new EncomendaRepository();
class EncomendaService {
    constructor() {

    }

createEncomenda(res, encomenda) {
    try {
      
        _encomenda.create(res,encomenda)
        
    } catch (error) {
    }

}

getEncomendas (res)  {
    try {
      return  _encomenda.get(res);
    } catch (error) {
    }
}



getEncomendaById (req, res) {
    return _encomenda.getByID(req, res);
}



deleteEncomenda(res, id) {
   
    return _encomenda.delete(res, id);
}

updateEncomenda(req, res){
    return _encomenda.update(req,res);
}
}
module.exports = EncomendaService;