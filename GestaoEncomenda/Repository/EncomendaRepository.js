var mongoose = require('mongoose');

const Encomenda = require('../models/encomenda');

class EncomendaRepository {
    constructor() {

    }

    get(res) {
        Encomenda.find()
            .exec({})
            .then(docs => {
                console.log("DB: \n" + docs);
                if (docs.length >= 0) {
                    return res.status(200).json(docs);
                } else {
                    return res.status(404).json({message: 'No orders found.'})
                }
            })
            .catch(err => {
                console.log(err);
                return res.status(500).json({error: err});
            });
    }

    getByID(req, res) {
        const id = req.params.ID;
        Encomenda.findById(id)
            .exec({})
            .then(doc => {
                console.log("DB: \n" + doc);
                if (doc) {
                    res.status(200).json(doc);
                } else {
                    res.status(404).json({message: 'No object found with gived ID'})
                }

            })
            .catch(err => {
                console.log(err);
                res.status(500).json({error: err});
            });
    }

    

    create(res, encomenda) {
        encomenda.save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    created_encomenda: encomenda
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json(
                    {
                        message: "There was a problem registering order",
                        error: err
                    });
            });

    }

    delete(res, id) {
        Encomenda.deleteOne({
            _id: id
        })
            .exec({})
            .then(doc => {
                res.status(200).json(doc);
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({error: err});
            });
    }
    update (req, res)  {
      
        // Find note and update it with the request body
        Encomenda.findByIdAndUpdate(req.params.id, {
                
                User: req.body.User,
                idProduto:req.body.idProduto,
                quantidade:req.body.quantidade,
                nomeProduto:req.body.nomeProduto
            } , {new: true})
        .then(encomenda => {
            if(!encomenda) {
                return res.status(404).send({
                    message: "Order not found with id " + req.params.id
                });
            }
            res.send(encomenda);
        }).catch(err => {
            if(err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Encomenda not found with id " + req.params.id
                });                
            }
            return res.status(500).send({
                message: "Error updating order with id " + req.params.id
            });
        });
    };
}

module.exports = EncomendaRepository;



