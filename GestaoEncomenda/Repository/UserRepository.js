var mongoose = require('mongoose');

const User = require('../models/user');

class UserRepository {
    constructor() {

    }

    get(res) {
        User.find()
            .exec({})
            .then(docs => {
                console.log("DB: \n" + docs);
                if (docs.length >= 0) {
                    return res.status(200).json(docs);
                } else {
                    return res.status(404).json({message: 'No users found.'})
                }
            })
            .catch(err => {
                console.log(err);
                return res.status(500).json({error: err});
            });
    }

    getByID(req, res) {
        const id = req.params.ID;
        User.findById(id)
            .exec({})
            .then(doc => {
                console.log("DB: \n" + doc);
                if (doc) {
                    res.status(200).json(doc);
                } else {
                    res.status(404).json({message: 'No object found with gived ID'})
                }

            })
            .catch(err => {
                console.log(err);
                res.status(500).json({error: err});
            });
    }

    g

    create(res, user) {
        user.save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    created_user: user
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json(
                    {
                        message: "There was a problem registering client",
                        error: err
                    });
            });

    }

    delete(res, id) {
        User.deleteOne({
            _id: id
        })
            .exec({})
            .then(doc => {
                res.status(200).json(doc);
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({error: err});
            });
    }

    update (req, res)  {
      
        // Find note and update it with the request body
        User.findByIdAndUpdate(req.params.id, {
            nome: req.body.nome,
            username: req.body.username,
            password: req.body.password,
            role: req.body.role,
              
            } , {new: true})
        .then(user => {
            if(!user) {
                return res.status(404).send({
                    message: "User not found with id " + req.params.id
                });
            }
            res.send(user);
        }).catch(err => {
            if(err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.id
                });                
            }
            return res.status(500).send({
                message: "Error updating user with id " + req.params.id
            });
        });
    };
}

module.exports = UserRepository;

