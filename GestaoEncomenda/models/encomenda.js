const mongoose = require('mongoose');
const User = require('./user');
//const Schema = mongoose.Schema;

const encomendaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    User: {type: mongoose.Schema.Types.ObjectId, ref: User},
    Data: {type: Date, default: Date.now}, 
    idProduto: {type: Number, required:true},
    quantidade: {type: Number, required:true},
    nomeProduto: {type: String, required:true}
});

//Export the model
module.exports = mongoose.model('Encomenda', encomendaSchema);