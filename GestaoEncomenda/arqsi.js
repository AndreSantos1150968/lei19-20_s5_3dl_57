var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var cors = require('cors');
app.use(cors());
var port = process.env.PORT || 1234;       

var router = express.Router();             
const database = require('./Repository/Database');
const user= require('./Routes/UserRoute');
const encomenda= require('./Routes/EncomendaRoute');
// START THE SERVER
// =============================================================================

app.use('/user',user);
app.use('/encomenda', encomenda);
app.listen(port);
console.log('Magic happens on port ' + port);
