var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const Encomenda= require('../models/encomenda')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var router = express.Router();

var service = require('../Services/EncomendaService');
var _serv= new service();

router.get('/', (req, res, next) => {
    _serv.getEncomendas(res);
});

router.get('/:ID', (req, res, next) => {
    _serv.getEncomendaById(req, res);
});


router.post('/', (req, res, next) => {
    
 
    const encomenda = new Encomenda({
        _id : new mongoose.Types.ObjectId(),
        User: req.body.User,
        idProduto:req.body.idProduto,
        quantidade:req.body.quantidade,
        nomeProduto: req.body.nomeProduto

    })

    _serv.createEncomenda(res, encomenda);
});

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    _serv.deleteEncomenda(res, id);
});

router.put('/:id', (req, res, next)=> {
 
    _serv.updateEncomenda(req,res);

});

module.exports = router;
