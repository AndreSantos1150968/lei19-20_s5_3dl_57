var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const User= require('../models/user')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var router = express.Router();

var service = require('../Services/UserService');
var _serv= new service();

router.get('/', (req, res, next) => {
    _serv.getUsers(res);
});

router.get('/:ID', (req, res, next) => {
    _serv.getUsersById(req, res);
});


router.post('/', (req, res, next) => {
    const user = new User({
        _id : new mongoose.Types.ObjectId(),
        nome: req.body.nome,
        username: req.body.username,
        password: req.body.password,
        role: req.body.role,
    })

    _serv.createUser(res, user);
});

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    _serv.deleteUser(res, id);
});

router.put('/:id', (req, res, next)=> {
 
    _serv.updateUser(req,res);

});
module.exports = router;
