using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.ModelsDTO;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaquinasController : ControllerBase
    {
        private readonly MasterDataFabricaContext _context;

        public MaquinasController(MasterDataFabricaContext context)
        {
            _context = context;
            TipoMaquina[] listaTiposMaquina = _context.TipoMaquinaDB.ToArray();
            Operacao[] listaOperacoes = _context.OperacaoDB.ToArray();
        }


        // GET: api/Maquina
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MaquinaDTO>>> GetMaquinas()
        {
            Maquina[] lista = await _context.MaquinaDB.ToArrayAsync();
            List<MaquinaDTO> listaMaquinaDTO = new List<MaquinaDTO>();
            foreach(Maquina m in lista){
                listaMaquinaDTO.Add(new MaquinaDTO(m));
            }

            return listaMaquinaDTO;
        }


        // GET: api/Maquina/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Maquina>> GetMaquina(long id)
        {
            var maquina = await _context.MaquinaDB.FindAsync(id);

            if (maquina == null)
            {
                return NotFound();
            }

            //MaquinaDTO maquinaDto = new MaquinaDTO(maquina);
            return maquina;
        }


        // POST: api/Maquina
        [HttpPost]
        public async Task<ActionResult<Maquina>> PostMaquina(Maquina item)
        {
            //Verifica se o TipoMaquina existe
            var tipoMaquina = await _context.TipoMaquinaDB.FindAsync(item.TipoMaquinaId);

            if(tipoMaquina == null){
                return BadRequest("Tipo de maquina não existe!");
            }

            item.TipoMaquina = tipoMaquina;

            _context.MaquinaDB.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMaquina), new { id = item.Id }, item);
        }


        // PUT: api/Maquina/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaquina(long id, Maquina item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            //Verifica se o TipoMaquina existe
            var tipoMaquina = await _context.TipoMaquinaDB.FindAsync(item.TipoMaquinaId);

            if(tipoMaquina == null){
                return BadRequest("Tipo de maquina não existe!");
            }

            item.TipoMaquina = tipoMaquina;

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }


        // DELETE: api/Maquina/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaquina(long id)
        {
            var maquina = await _context.MaquinaDB.FindAsync(id);

            if (maquina == null)
            {
                return NotFound();
            }

            _context.MaquinaDB.Remove(maquina);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
