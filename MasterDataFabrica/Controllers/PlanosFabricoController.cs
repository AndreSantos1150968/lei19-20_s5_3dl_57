using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanosFabricoController : ControllerBase
    {
        private readonly MasterDataFabricaContext _context;

        public PlanosFabricoController(MasterDataFabricaContext context)
        {
            _context = context;
            Operacao[] listaOperacoes = _context.OperacaoDB.ToArray();
        }


        // GET: api/PlanoFabrico
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlanoFabrico>>> GetPlanosFabrico()
        {
            return await _context.PlanoFabricoDB.ToListAsync();
        }


        // GET: api/PlanoFabrico/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlanoFabrico>> GetPlanoFabrico(long id)
        {
            var planoFabrico = await _context.PlanoFabricoDB.FindAsync(id);

            if (planoFabrico == null)
            {
                return NotFound();
            }

            return planoFabrico;
        }

        // GET: api/PlanoFabrico/5/Operacoes
        [HttpGet("{id}/operacoes")]
        public async Task<ActionResult<List<Operacao>>> GetOperacoesPlanoFabrico(long id)
        {
            var planoFabrico = await _context.PlanoFabricoDB.FindAsync(id);

            if (planoFabrico == null)
            {
                return NotFound();
            }

            return planoFabrico.ListaOperacoes;
        }
 
        // POST: api/PlanoFabrico
        [HttpPost]
        public async Task<ActionResult<PlanoFabrico>> PostPlanoFabrico(PlanoFabrico item)
        {
            //Verifica se existe a operacao
            bool confirma=false;
            List<Operacao> operacoes = new List<Operacao>();
            long tam = item.ListaOperacoes.Count();
            long tamReal = 0; 
            foreach(Operacao o in item.ListaOperacoes){
                foreach(Operacao op in _context.OperacaoDB){
                    if(op.Nome.Equals(o.Nome)){
                        operacoes.Add(op);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Operacao não existe");
            }
            
            item.ListaOperacoes = null;
            item.ListaOperacoes=operacoes;
 
            _context.PlanoFabricoDB.Add(item);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction(nameof(GetPlanoFabrico), new { id = item.Id }, item);
        }


        // PUT: api/PlanoFabrico/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlanoFabrico(long id, PlanoFabrico item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }
 
            //Verifica se existem as operacoes
            bool confirma=false;
            List<Operacao> operacoes = new List<Operacao>();
            long tam = item.ListaOperacoes.Count();
            long tamReal = 0; 
            foreach(Operacao o in item.ListaOperacoes){
                foreach(Operacao op in _context.OperacaoDB){
                    if(op.Nome.Equals(o.Nome)){
                        operacoes.Add(op);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Operacao não existe");
            }

        
            var pFabrico = _context.PlanoFabricoDB.FirstOrDefault(e => e.Id == id);
            if (pFabrico == null)
            {
                return BadRequest("Erro!");
            }
            else
            {
                pFabrico.NomePlano = item.NomePlano;
                pFabrico.ListaOperacoes = operacoes;
                await _context.SaveChangesAsync();
                //return Ok(item);
                return NoContent();
            }
        }


        // DELETE: api/PlanoFabrico/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlanoFabrico(long id)
        {
            var planoFabrico = await _context.PlanoFabricoDB.FindAsync(id);

            if (planoFabrico == null)
            {
                return NotFound();
            }

            _context.PlanoFabricoDB.Remove(planoFabrico);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
