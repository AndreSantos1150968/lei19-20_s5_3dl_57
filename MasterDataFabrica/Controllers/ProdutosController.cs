using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.ModelsDTO;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosController : ControllerBase
    {
       private readonly MasterDataFabricaContext _context;

        public ProdutosController(MasterDataFabricaContext context)
        {
            _context = context;
            PlanoFabrico[] listaPlanos = _context.PlanoFabricoDB.ToArray();
            Operacao[] listaOperacoes = _context.OperacaoDB.ToArray();
        }


        // GET: api/Produto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProdutoDTO>>> GetProdutos()
        {
            Produto[] lista = await _context.ProdutoDB.ToArrayAsync();
            List<ProdutoDTO> listaProdutoDTO = new List<ProdutoDTO>();
            foreach(Produto p in lista){
                listaProdutoDTO.Add(new ProdutoDTO(p));
            }

            return listaProdutoDTO;
        }


        // GET: api/Produto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> GetProduto(long id)
        {
            var produto = await _context.ProdutoDB.FindAsync(id);

            if (produto == null)
            {
                return NotFound();
            }

            return produto;
        }


        // GET: api/Produto/5/PlanoFabrico
        [HttpGet("{id}/PlanoFabrico")]
        public async Task<ActionResult<PlanoFabrico>> GetPlanoFabricoProduto(long id)
        {
            var produto = await _context.ProdutoDB.FindAsync(id);

            if (produto == null)
            {
                return NotFound();
            }

            return produto.PlanoFabrico;
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<ActionResult<Produto>> PostProduto(Produto item)
        {
            //Verifica se o plano de fabrico existe
            var planoFabrico = await _context.PlanoFabricoDB.FindAsync(item.PlanoFabricoId);

            if(planoFabrico == null){
                return BadRequest("Plano de Fabrico não existe!");
            }

            item.PlanoFabrico = planoFabrico;

            _context.ProdutoDB.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetProduto), new { id = item.Id }, item);
        }


        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto(long id, Produto item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

             //Verifica se o plano de fabrico existe
            var planoFabrico = await _context.PlanoFabricoDB.FindAsync(item.PlanoFabricoId);

            if(planoFabrico == null){
                return BadRequest("Plano de Fabrico não existe!");
            }

            item.PlanoFabrico = planoFabrico;

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }


        // DELETE: api/Produto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduto(long id)
        {
            var produto = await _context.ProdutoDB.FindAsync(id);

            if (produto == null)
            {
                return NotFound();
            }

            _context.ProdutoDB.Remove(produto);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
