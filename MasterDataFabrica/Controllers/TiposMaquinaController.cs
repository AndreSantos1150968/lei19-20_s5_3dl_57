using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.ModelsDTO;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TiposMaquinaController : ControllerBase
    {
        private readonly MasterDataFabricaContext _context;
        public TiposMaquinaController(MasterDataFabricaContext context)
        {
            _context = context;
            Operacao[] listaOperacoes = _context.OperacaoDB.ToArray();
        }

        // GET: api/TipoMaquina
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoMaquina>>> GetTipoMaquinas()
        {
            return await _context.TipoMaquinaDB.ToListAsync();
        }


        // GET: api/TipoMaquina/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoMaquina>> GetTipoMaquina(long id)
        {
            var tmaquina = await _context.TipoMaquinaDB.FindAsync(id);

            if (tmaquina == null)
            {
                return NotFound();
            }

            return tmaquina;
        }

        // GET: api/TipoMaquina/5/Operacoes
        [HttpGet("{id}/operacoes")]
        public async Task<ActionResult<List<Operacao>>> GetOperacoesTipoMaquina(long id)
        {
            var tipoMaquina = await _context.TipoMaquinaDB.FindAsync(id);

            if (tipoMaquina == null)
            {
                return NotFound();
            }

            return tipoMaquina.ListaOperacoes;
        }

        // GET: api/TipoMaquina/5/Maquinas
        [HttpGet("{id}/maquinas")]
        public async Task<ActionResult<List<MaquinaDTO>>> GetMaquinasTipoMaquina(long id)
        {
            TipoMaquina[] listaTiposMaquina = _context.TipoMaquinaDB.ToArray();
            var maquinas = await _context.MaquinaDB.ToArrayAsync();
            List<MaquinaDTO> lst = new List<MaquinaDTO>();
            foreach(Maquina m in maquinas){
                if(m.TipoMaquinaId == id){
                    lst.Add(new MaquinaDTO(m));
                }
            }

            if (lst == null)
            {
                return NotFound("Este tipo de maquina não tem maquinas associadas");
            }    
            
            return lst;
        }

        // POST: api/TipoMaquina
        [HttpPost]
        public async Task<ActionResult<TipoMaquina>> PostTipoMaquina(TipoMaquina item)
        {
            //Verifica se existe operacao
            bool confirma=false;
            List<Operacao> operacoes = new List<Operacao>();
            long tam = item.ListaOperacoes.Count();
            long tamReal = 0; 
            foreach(Operacao o in item.ListaOperacoes){
                foreach(Operacao op in _context.OperacaoDB){
                    if(op.Nome.Equals(o.Nome)){
                        operacoes.Add(op);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Operacao não existe");
            }

            item.ListaOperacoes=null;
            item.ListaOperacoes=operacoes;

           
            _context.TipoMaquinaDB.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTipoMaquina), new { id = item.Id }, item);
        }


        // PUT: api/TipoMaquina/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoMaquina(long id, TipoMaquina item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            //Verifica se existem operacoes
            bool confirma=false;
            List<Operacao> operacoes = new List<Operacao>();
            long tam = item.ListaOperacoes.Count();
            long tamReal = 0; 
            foreach(Operacao o in item.ListaOperacoes){
                foreach(Operacao op in _context.OperacaoDB){
                    if(op.Nome.Equals(o.Nome)){
                        operacoes.Add(op);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Operacao não existe");
            }

            var tMaquina = _context.TipoMaquinaDB.FirstOrDefault(t => t.Id == id);
            if (tMaquina == null)
            {
                return BadRequest("Erro!");
            }
            else
            {
                tMaquina.Descricao = item.Descricao;
                tMaquina.ListaOperacoes = operacoes;
                await _context.SaveChangesAsync();
                //return Ok(item);
                return NoContent();
            }
        }


        // DELETE: api/TipoMaquina/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTipoMaquina(long id)
        {
            var tmaquina = await _context.TipoMaquinaDB.FindAsync(id);

            if (tmaquina == null)
            {
                return NotFound();
            }

            _context.TipoMaquinaDB.Remove(tmaquina);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
