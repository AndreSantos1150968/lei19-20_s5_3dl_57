using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperacoesController : ControllerBase
    {
       private readonly MasterDataFabricaContext _context;

        public OperacoesController(MasterDataFabricaContext context)
        {
            _context = context;            
        }


        // GET: api/Operacao
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Operacao>>> GetOperacoes()
        {
            return await _context.OperacaoDB.ToListAsync();
        }


        // GET: api/Operacao/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Operacao>> GetOperacao(long id)
        {
            var operacao = await _context.OperacaoDB.FindAsync(id);

            if (operacao == null)
            {
                return NotFound();
            }

            return operacao;
        }


        // POST: api/Operacao
        [HttpPost]
        public async Task<ActionResult<Operacao>> PostOperacao(Operacao item)
        {
            _context.OperacaoDB.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetOperacao), new { id = item.Id }, item);
        }


        // PUT: api/Operacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOperacao(long id, Operacao item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }


        // DELETE: api/Operacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOperacao(long id)
        {
            var operacao = await _context.OperacaoDB.FindAsync(id);

            if (operacao == null)
            {
                return NotFound();
            }

            _context.OperacaoDB.Remove(operacao);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
