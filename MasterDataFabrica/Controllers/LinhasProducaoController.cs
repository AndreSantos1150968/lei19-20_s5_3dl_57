using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.ModelsDTO;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinhasProducaoController : ControllerBase
    {
       private readonly MasterDataFabricaContext _context;

        public LinhasProducaoController(MasterDataFabricaContext context)
        {
            _context = context;
            Maquina[] listaMaquinas = _context.MaquinaDB.ToArray();
            TipoMaquina[] listaTiposMaquina = _context.TipoMaquinaDB.ToArray();
            Operacao[] listaOperacoes = _context.OperacaoDB.ToArray();
        }


        // GET: api/LinhaProducao
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LinhaProducaoDTO>>> GetLinhasProducao()
        {
            LinhaProducao[] linhaProducao = await _context.LinhaProducaoDB.ToArrayAsync();
            List<LinhaProducaoDTO> linhaProducaoDTO = new List<LinhaProducaoDTO>();
            foreach(LinhaProducao lp in linhaProducao){
                linhaProducaoDTO.Add(new LinhaProducaoDTO(lp));
            } 
            return linhaProducaoDTO;
        }


        // GET: api/LinhaProducao/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LinhaProducao>> GetLinhaProducao(long id)
        {
            var linhaProducao = await _context.LinhaProducaoDB.FindAsync(id);

            if (linhaProducao == null)
            {
                return NotFound();
            }

            return linhaProducao;
        }


        // POST: api/LinhaProducao
        [HttpPost]
        public async Task<ActionResult<LinhaProducao>> PostLinhaProducao(LinhaProducao item)
        {
            //Verifica se existe a maquina
            bool confirma=false;
            List<Maquina> maquinas = new List<Maquina>();
            long tam = item.ListaMaquinas.Count();
            long tamReal = 0; 
            foreach(Maquina m in item.ListaMaquinas){
                foreach(Maquina mq in _context.MaquinaDB){
                    if(m.NomeMaquina.Equals(mq.NomeMaquina)){
                        maquinas.Add(mq);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Maquina não existe");
            }
            
            item.ListaMaquinas = null;
            item.ListaMaquinas=maquinas;
 
            _context.LinhaProducaoDB.Add(item);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction(nameof(GetLinhaProducao), new { id = item.Id}, item);
        
        }


        // PUT: api/LinhaProducao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLinhaProducao(long id, LinhaProducao item)
        {
            if (id != item.Id)
            {
                return BadRequest("Id's não compativéis!");
            }
 
            //Verifica se existe a maquina
            bool confirma=false;
            List<Maquina> maquinas = new List<Maquina>();
            long tam = item.ListaMaquinas.Count();
            long tamReal = 0; 
            foreach(Maquina m in item.ListaMaquinas){
                foreach(Maquina mq in _context.MaquinaDB){
                    if(m.NomeMaquina.Equals(mq.NomeMaquina)){
                        maquinas.Add(mq);
                        tamReal++;
                    }
                }
            }
            if(tam == tamReal)
                confirma=true;

            if(!confirma){
                return BadRequest("Maquina não existe");
            }
            
        
            var linhaProducao = _context.LinhaProducaoDB.FirstOrDefault(e => e.Id == id);
            if (linhaProducao == null)
            {
                return BadRequest("Erro!");
            }
            else
            {
                linhaProducao.NomeLinhaProducao = item.NomeLinhaProducao;
                linhaProducao.ListaMaquinas = maquinas;
                await _context.SaveChangesAsync();
                //return Ok(item);
                return NoContent();
            }
        }


        // DELETE: api/LinhaProducao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLinhaProducao(long id)
        {
            var linhaProducao = await _context.LinhaProducaoDB.FindAsync(id);

            if (linhaProducao == null)
            {
                return NotFound();
            }

            _context.LinhaProducaoDB.Remove(linhaProducao);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
