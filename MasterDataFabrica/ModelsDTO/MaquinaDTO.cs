using MasterDataFabrica.Models;

namespace MasterDataFabrica.ModelsDTO
{
    public class MaquinaDTO
    {
       public long MaquinaDTOId { get; set; }
        public string MaquinaDTONome { get; set;}
        public string ModeloDTO { get; set; }
        public string TipoMaquinaDTO {get;set;}
        public MaquinaDTO(Maquina maquina){
            MaquinaDTOId = maquina.Id;
            MaquinaDTONome = maquina.NomeMaquina;
            ModeloDTO = maquina.Modelo;
            TipoMaquinaDTO = maquina.TipoMaquina.Descricao;
        }
    }
}