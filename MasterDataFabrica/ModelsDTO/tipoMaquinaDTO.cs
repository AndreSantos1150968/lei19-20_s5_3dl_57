using MasterDataFabrica.Models;

namespace MasterDataFabrica.ModelsDTO
{
    public class TipoMaquinaDTO
    {
        public long TipoMaquinaDTOId { get; set; }
        public string TipoMaquinaDTONome { get; set; }
        public TipoMaquinaDTO(TipoMaquina tipoMaquina){
            TipoMaquinaDTOId =tipoMaquina.Id;
            TipoMaquinaDTONome = tipoMaquina.Descricao;
        }

    }
}