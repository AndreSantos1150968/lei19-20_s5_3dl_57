using MasterDataFabrica.Models;

namespace MasterDataFabrica.ModelsDTO
{
    public class PlanoFabricoDTO
    {
        public long PlanoFabricoDTOId { get; set; }
        public string PlanoFabricoDTONome { get; set;}
        public PlanoFabricoDTO(PlanoFabrico planoFabrico)
        {
            PlanoFabricoDTOId = planoFabrico.Id;
            PlanoFabricoDTONome = planoFabrico.NomePlano;
        }    
    }
}