using MasterDataFabrica.Models;

namespace MasterDataFabrica.ModelsDTO
{
    public class ProdutoDTO
    {
        public long ProdutoDTOId { get; set; }
        public string ProdutoDTONome { get; set; }

        public string PlanoFabricoDTO { get; set; }

        public ProdutoDTO(Produto prod){
            ProdutoDTOId= prod.Id;
            ProdutoDTONome = prod.Nome;
            PlanoFabricoDTO = prod.PlanoFabrico.NomePlano;
        }
    }
}