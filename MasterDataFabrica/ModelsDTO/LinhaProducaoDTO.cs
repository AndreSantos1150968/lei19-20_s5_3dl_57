using MasterDataFabrica.Models;

namespace MasterDataFabrica.ModelsDTO
{
    public class LinhaProducaoDTO
    {
        public long LinhaProducaoDTOId { get; set; }
        public string LinhaProducaoDTONome { get; set; }
        public LinhaProducaoDTO(LinhaProducao linha){
            LinhaProducaoDTOId = linha.Id;
            LinhaProducaoDTONome = linha.NomeLinhaProducao;
        }
    }
}