namespace MasterDataFabrica.Models
{
    public class Maquina
    {
        public long Id { get; set; }
        public string NomeMaquina { get; set; }
        public string Modelo { get; set; }
        public long TipoMaquinaId { get; set; }
        public TipoMaquina TipoMaquina { get; set;}
    }
}