using System.Collections.Generic;

namespace MasterDataFabrica.Models
{
    public class TipoMaquina
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public List<Operacao> ListaOperacoes{ get; set; }

    }

}