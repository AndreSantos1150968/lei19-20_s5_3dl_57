using System.Collections.Generic;

namespace MasterDataFabrica.Models
{
    public class Produto
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long PlanoFabricoId {get;set;}
        public PlanoFabrico PlanoFabrico{ get; set; }

    }
}