using System.Collections.Generic;

namespace MasterDataFabrica.Models
{
    public class LinhaProducao
    {
        public long Id { get; set; }
        public string NomeLinhaProducao { get; set; }
        public List<Maquina> ListaMaquinas{ get; set; }   
    }
}