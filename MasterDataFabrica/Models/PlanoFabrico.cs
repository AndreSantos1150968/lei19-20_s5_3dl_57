using System.Collections.Generic;

namespace MasterDataFabrica.Models
{
    public class PlanoFabrico
    {
        public long Id { get; set; }
        public string NomePlano { get; set; }
        public List<Operacao> ListaOperacoes { get; set; }
    }
}