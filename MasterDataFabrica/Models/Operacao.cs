namespace MasterDataFabrica.Models
{
    public class Operacao
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public int TempoExecucao { get; set; }
        public string Ferramenta{ get; set; }
    }
}