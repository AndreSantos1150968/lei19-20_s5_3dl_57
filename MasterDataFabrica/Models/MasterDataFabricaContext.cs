using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace MasterDataFabrica.Models
{
    public class MasterDataFabricaContext : DbContext
    {
        public MasterDataFabricaContext(DbContextOptions<MasterDataFabricaContext> options)
            : base(options)
        {
        }
        public DbSet<Produto> ProdutoDB { get; set; }
        public DbSet<Operacao> OperacaoDB { get; set; }
        public DbSet<LinhaProducao> LinhaProducaoDB { get; set; }
        public DbSet<Maquina> MaquinaDB { get; set; }
        public DbSet<TipoMaquina> TipoMaquinaDB { get; set; }
        public DbSet<PlanoFabrico> PlanoFabricoDB { get; set; }
        
        }
}