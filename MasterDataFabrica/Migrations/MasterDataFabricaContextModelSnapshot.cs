﻿// <auto-generated />
using System;
using MasterDataFabrica.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MasterDataFabrica.Migrations
{
    [DbContext(typeof(MasterDataFabricaContext))]
    partial class MasterDataFabricaContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MasterDataFabrica.Models.LinhaProducao", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("NomeLinhaProducao")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("LinhaProducaoDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Maquina", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long?>("LinhaProducaoId")
                        .HasColumnType("bigint");

                    b.Property<string>("Modelo")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("NomeMaquina")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("TipoMaquinaId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("LinhaProducaoId");

                    b.HasIndex("TipoMaquinaId");

                    b.ToTable("MaquinaDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Operacao", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Ferramenta")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Nome")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("PlanoFabricoId")
                        .HasColumnType("bigint");

                    b.Property<int>("TempoExecucao")
                        .HasColumnType("int");

                    b.Property<long?>("TipoMaquinaId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("PlanoFabricoId");

                    b.HasIndex("TipoMaquinaId");

                    b.ToTable("OperacaoDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.PlanoFabrico", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("NomePlano")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("PlanoFabricoDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Produto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Nome")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("PlanoFabricoId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("PlanoFabricoId");

                    b.ToTable("ProdutoDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.TipoMaquina", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("TipoMaquinaDB");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Maquina", b =>
                {
                    b.HasOne("MasterDataFabrica.Models.LinhaProducao", null)
                        .WithMany("ListaMaquinas")
                        .HasForeignKey("LinhaProducaoId");

                    b.HasOne("MasterDataFabrica.Models.TipoMaquina", "TipoMaquina")
                        .WithMany()
                        .HasForeignKey("TipoMaquinaId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Operacao", b =>
                {
                    b.HasOne("MasterDataFabrica.Models.PlanoFabrico", null)
                        .WithMany("ListaOperacoes")
                        .HasForeignKey("PlanoFabricoId");

                    b.HasOne("MasterDataFabrica.Models.TipoMaquina", null)
                        .WithMany("ListaOperacoes")
                        .HasForeignKey("TipoMaquinaId");
                });

            modelBuilder.Entity("MasterDataFabrica.Models.Produto", b =>
                {
                    b.HasOne("MasterDataFabrica.Models.PlanoFabrico", "PlanoFabrico")
                        .WithMany()
                        .HasForeignKey("PlanoFabricoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
