﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFabrica.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LinhaProducaoDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeLinhaProducao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhaProducaoDB", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanoFabricoDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomePlano = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanoFabricoDB", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoMaquinaDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMaquinaDB", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: true),
                    PlanoFabricoId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoDB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProdutoDB_PlanoFabricoDB_PlanoFabricoId",
                        column: x => x.PlanoFabricoId,
                        principalTable: "PlanoFabricoDB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MaquinaDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeMaquina = table.Column<string>(nullable: true),
                    Modelo = table.Column<string>(nullable: true),
                    TipoMaquinaId = table.Column<long>(nullable: false),
                    LinhaProducaoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaquinaDB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaquinaDB_LinhaProducaoDB_LinhaProducaoId",
                        column: x => x.LinhaProducaoId,
                        principalTable: "LinhaProducaoDB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MaquinaDB_TipoMaquinaDB_TipoMaquinaId",
                        column: x => x.TipoMaquinaId,
                        principalTable: "TipoMaquinaDB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperacaoDB",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: true),
                    TempoExecucao = table.Column<int>(nullable: false),
                    Ferramenta = table.Column<string>(nullable: true),
                    PlanoFabricoId = table.Column<long>(nullable: true),
                    TipoMaquinaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperacaoDB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperacaoDB_PlanoFabricoDB_PlanoFabricoId",
                        column: x => x.PlanoFabricoId,
                        principalTable: "PlanoFabricoDB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OperacaoDB_TipoMaquinaDB_TipoMaquinaId",
                        column: x => x.TipoMaquinaId,
                        principalTable: "TipoMaquinaDB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaquinaDB_LinhaProducaoId",
                table: "MaquinaDB",
                column: "LinhaProducaoId");

            migrationBuilder.CreateIndex(
                name: "IX_MaquinaDB_TipoMaquinaId",
                table: "MaquinaDB",
                column: "TipoMaquinaId");

            migrationBuilder.CreateIndex(
                name: "IX_OperacaoDB_PlanoFabricoId",
                table: "OperacaoDB",
                column: "PlanoFabricoId");

            migrationBuilder.CreateIndex(
                name: "IX_OperacaoDB_TipoMaquinaId",
                table: "OperacaoDB",
                column: "TipoMaquinaId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoDB_PlanoFabricoId",
                table: "ProdutoDB",
                column: "PlanoFabricoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaquinaDB");

            migrationBuilder.DropTable(
                name: "OperacaoDB");

            migrationBuilder.DropTable(
                name: "ProdutoDB");

            migrationBuilder.DropTable(
                name: "LinhaProducaoDB");

            migrationBuilder.DropTable(
                name: "TipoMaquinaDB");

            migrationBuilder.DropTable(
                name: "PlanoFabricoDB");
        }
    }
}
