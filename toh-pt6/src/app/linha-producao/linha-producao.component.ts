import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  
import { MatTableDataSource} from '@angular/material';
import { MatPaginator} from '@angular/material/paginator';
import { MaquinaService } from '../maquina.service';  
import { Maquina } from '../maquina';
import { LinhaProducaoService } from '../linha-producao.service';   
import { LinhaProducao } from '../linha-producao';

@Component({
  selector: 'app-linha-producao',
  templateUrl: './linha-producao.component.html',
  styleUrls: ['./linha-producao.component.css']
})
export class LinhaProducaoComponent implements OnInit, AfterViewInit  {
  
  public displayedColumns = ['Id', 'Nome', 'Máquinas', 'update', 'delete'];
  public dataSource = new MatTableDataSource<LinhaProducao>();
  alunoForm: any;  
  allAlunos: Observable<LinhaProducao[]>;  
  alunoIdUpdate = null;
  allOperacoes:Maquina[]; 
  selectedMaquinas: Maquina[];
  operacoes = new FormControl();
  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator

  constructor(private formbulider: FormBuilder, private maquinaService: MaquinaService, private linhaProducaoService: LinhaProducaoService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      linhaProducaoNome: ['', [Validators.required]],
      listaMaquinas : [this.selectedMaquinas,[Validators.required]],
    });  
    this.loadAllAlunos();
    this.loadAllOperacoes()
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  loadAllAlunos() {  
    this.allAlunos = this.linhaProducaoService.getAllLinhaProducao();
    this.linhaProducaoService.getAllLinhaProducao().subscribe(res => 
      this.dataSource.data = res as LinhaProducao[]);  
  } 

  loadAllOperacoes() {  
    this.maquinaService.getAllMaquinas().subscribe(res => this.allOperacoes = res);  
  }
  
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    console.log(aluno); 
    this.alunoForm.reset();  
  } 
  CreateAluno(aluno: LinhaProducao) {  
    if (this.alunoIdUpdate == null) {  
      this.linhaProducaoService.createLinhaProducao(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.linhaProducaoId = this.alunoIdUpdate;  
      this.linhaProducaoService.updateLinhaProducao(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  loadAlunoToEdit(alunoid: number) {  
    this.linhaProducaoService.getLinhaProducaoById(alunoid).subscribe(aluno=> {  
      this.alunoIdUpdate = aluno.linhaProducaoId;  
      this.alunoForm.controls['linhaProducaoNome'].setValue(aluno.linhaProducaoNome);

    });    
  }  
  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar esta Linha de Produção?")) {   
      this.linhaProducaoService.deleteLinhaProducaoById(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 

}
