import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { PlanoFabrico } from './plano-fabrico'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};


@Injectable({
  providedIn: 'root'
})
export class PlanoFabricoService {

  url = 'https://localhost:5001/api/PlanoFabrico';  

  constructor(private http: HttpClient) { }

  getAllPlanoFabrico(): Observable<PlanoFabrico[]> {  
    return this.http.get<PlanoFabrico[]>(this.url);  
  }  
  getPlanoFabricoById(PlanoFabricoid: number): Observable<PlanoFabrico> {  
    const apiurl = `${this.url}/${PlanoFabricoid}`;
    return this.http.get<PlanoFabrico>(apiurl);  
  } 
  createPlanoFabrico(PlanoFabrico: PlanoFabrico): Observable<PlanoFabrico> {  
    return this.http.post<PlanoFabrico>(this.url, PlanoFabrico, httpOptions);  
  }  
  updatePlanoFabrico(PlanoFabricoid: number, PlanoFabrico: PlanoFabrico): Observable<PlanoFabrico> {  
    const apiurl = `${this.url}/${PlanoFabricoid}`;
    return this.http.put<PlanoFabrico>(apiurl,PlanoFabrico, httpOptions);  
  }  
  deletePlanoFabricoId(PlanoFabricoid: number): Observable<number> {  
    const apiurl = `${this.url}/${PlanoFabricoid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  }
}
