import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Operacao } from './operacao'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};


@Injectable({
  providedIn: 'root'
})
export class OperacaoService {

  url = 'https://localhost:5001/api/Operacao';  

  constructor(private http: HttpClient) { }

  getAllOperacaoes(): Observable<Operacao[]> {  
    return this.http.get<Operacao[]>(this.url);  
  }  
  getOperacaoById(operacaoid: number): Observable<Operacao> {  
    const apiurl = `${this.url}/${operacaoid}`;
    return this.http.get<Operacao>(apiurl);  
  } 
  createOperacao(operacao: Operacao): Observable<Operacao> {  
    return this.http.post<Operacao>(this.url, operacao, httpOptions);  
  }  
  updateOperacao(operacaoId: number, operacao: Operacao): Observable<Operacao> {  
    const apiurl = `${this.url}/${operacaoId}`;
    return this.http.put<Operacao>(apiurl,operacao, httpOptions);  
  }  
  deleteOperacaoById(operacaoid: number): Observable<number> {  
    const apiurl = `${this.url}/${operacaoid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  } 
}
