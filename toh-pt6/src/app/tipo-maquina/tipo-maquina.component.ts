import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  
import { MatTableDataSource } from '@angular/material';
import { TipoMaquinaService } from '../tipo-maquina.service';  
import { TipoMaquina } from '../tipo-maquina';
import { OperacaoService } from '../operacao.service';  
import { Operacao } from '../operacao';  

@Component({
  selector: 'app-tipo-maquina',
  templateUrl: './tipo-maquina.component.html',
  styleUrls: ['./tipo-maquina.component.css']
})
export class TipoMaquinaComponent implements OnInit {

 
  alunoForm: any;  
  allAlunos: Observable<TipoMaquina[]>;  
  alunoIdUpdate = null;
  allOperacoes:Operacao[];
  allOperacoes2:Operacao[];  
  selectedOperacoes;
  operacoes;
  selected2;

  constructor(private formbulider: FormBuilder, private tipoMaquinaService: TipoMaquinaService, private operacaoService: OperacaoService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      'tipoMaquinaNome': [null , [Validators.required]],
      'listaOperacoes' : [null ,[Validators.required]]
    });  

    this.loadAllAlunos();
    this.loadAllOperacoes()
  }

  loadAllAlunos() {  
    this.allAlunos = this.tipoMaquinaService.getAllTiposMaquina();  
  } 

  loadAllOperacoes() {  
    this.operacaoService.getAllOperacaoes().subscribe(res => this.allOperacoes = res);  
  }
  
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    console.log(aluno); 
    this.alunoForm.reset();  
  } 

  CreateAluno(aluno: TipoMaquina) {  
    if (this.alunoIdUpdate == null) {  
      this.tipoMaquinaService.createTipoMaquina(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.tipoMaquinaId = this.alunoIdUpdate;  
      this.tipoMaquinaService.updateTipoMaquina(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }

  loadAlunoToEdit(alunoid: number) {  
    this.tipoMaquinaService.getTipoMaquinaById(alunoid).subscribe(aluno=> {
      this.alunoIdUpdate = aluno.tipoMaquinaId;  
      this.alunoForm.controls['tipoMaquinaNome'].setValue(aluno.tipoMaquinaNome);
      this.allOperacoes2=aluno.listaOperacoes;
      console.log(this.allOperacoes2);
      console.log(this.allOperacoes);
      this.alunoForm.controls['listaOperacoes'].setValue(this.allOperacoes2);
    });    
  }
    
  compareObjects(user1: TipoMaquina, user2: TipoMaquina) {
    return user1 && user2 ? user1.tipoMaquinaId === user2.tipoMaquinaId : user1 === user2;
}

  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar este Tipo de Máquina ?")) {   
      this.tipoMaquinaService.deleteTipoMaquinaById(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 

}

