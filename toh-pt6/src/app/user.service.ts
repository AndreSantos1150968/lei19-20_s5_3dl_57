import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { User} from './user'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = 'https://localhost:5001/api/User';  // URL to web api
 

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {  
    return this.http.get<User[]>(this.userUrl);  
  }  
  getProdutoById(UserId: number): Observable<User> {  
    const apiurl = `${this.userUrl}/${UserId}`;
    return this.http.get<User>(apiurl);  
  } 
  createUser(user: User): Observable<User> {  
    return this.http.post<User>(this.userUrl, user, httpOptions);  
  }  
  updateProduto(UserId: number, User: User): Observable<User> {  
    const apiurl = `${this.userUrl}/${UserId}`;
    return this.http.put<User>(apiurl,UserId, httpOptions);  
  }  
 
}


