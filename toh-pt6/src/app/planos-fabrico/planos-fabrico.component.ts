import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  
import { MatTableDataSource } from '@angular/material';
import { TipoMaquinaService } from '../tipo-maquina.service';  
import { TipoMaquina } from '../tipo-maquina';
import { OperacaoService } from '../operacao.service';  
import { Operacao } from '../operacao';
import { PlanoFabricoService } from '../plano-fabrico.service';  
import { PlanoFabrico } from '../plano-fabrico';


@Component({
  selector: 'app-planos-fabrico',
  templateUrl: './planos-fabrico.component.html',
  styleUrls: ['./planos-fabrico.component.css']
})
export class PlanosFabricoComponent implements OnInit {

  alunoForm: any;  
  allAlunos: Observable<PlanoFabrico[]>;  
  alunoIdUpdate = null;
  allOperacoes:Operacao[]; 
  selectedYears: Operacao[];
  operacoes = new FormControl();

  constructor(private formbulider: FormBuilder, private operacaoService: OperacaoService, private planoFabricoService: PlanoFabricoService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      planoFabricoNome: ['', [Validators.required]],
      listaOperacoes : [this.selectedYears,[Validators.required]]
    
    });  
    this.loadAllAlunos();
    this.loadAllOperacoes()
  }
  loadAllAlunos() {  
    this.allAlunos = this.planoFabricoService.getAllPlanoFabrico();  
  } 


  loadAllOperacoes() {  
    this.operacaoService.getAllOperacaoes().subscribe(res => this.allOperacoes = res);  
  }
  
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    console.log(aluno); 
    this.alunoForm.reset();  
  } 
  CreateAluno(aluno: PlanoFabrico) {  
    if (this.alunoIdUpdate == null) {  
      this.planoFabricoService.createPlanoFabrico(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.planoFabricoId = this.alunoIdUpdate;  
      this.planoFabricoService.updatePlanoFabrico(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  loadAlunoToEdit(alunoid: number) {  
    this.planoFabricoService.getPlanoFabricoById(alunoid).subscribe(aluno=> {  
      this.alunoIdUpdate = aluno.planoFabricoId;  
      this.alunoForm.controls['planoFabricoNome'].setValue(aluno.planoFabricoNome);

    });    
  }  
  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar este Tipo de Máquina ?")) {   
      this.planoFabricoService.deletePlanoFabricoId(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 

}
