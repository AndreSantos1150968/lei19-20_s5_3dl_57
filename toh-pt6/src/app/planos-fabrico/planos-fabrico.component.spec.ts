import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanosFabricoComponent } from './planos-fabrico.component';

describe('PlanosFabricoComponent', () => {
  let component: PlanosFabricoComponent;
  let fixture: ComponentFixture<PlanosFabricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanosFabricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanosFabricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
