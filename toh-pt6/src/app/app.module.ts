import { NgModule }       from '@angular/core';
import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { OperacaoService } from './operacao.service';
import { TipoMaquinaService } from './tipo-maquina.service';
import { MaquinaService } from './maquina.service';
import { LinhaProducaoService } from './linha-producao.service';
import { ProdutoService } from './produto.service';
import { PlanoFabricoService } from './plano-fabrico.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { HeroesComponent }      from './heroes/heroes.component';
import { HeroSearchComponent }  from './hero-search/hero-search.component';
import { MessagesComponent }    from './messages/messages.component';
import { OperacaoComponent } from './operacao/operacao.component';
import { HomeComponent } from './home/home.component';
import { LinhaProducaoComponent } from './linha-producao/linha-producao.component';
import { MaquinasComponent } from './maquinas/maquinas.component';
import { ProdutosComponent } from './produtos/produtos.component';
import { PlanosFabricoComponent } from './planos-fabrico/planos-fabrico.component';

import {MatTableModule} from '@angular/material';
import {  
  MatButtonModule, MatMenuModule, MatDatepickerModule,MatNativeDateModule, MatCardModule, MatSidenavModule,MatFormFieldModule,  
  MatInputModule, MatTooltipModule, MatToolbarModule,MatListModule, MatSelectModule, MatOptionModule } from '@angular/material';  

import { MatRadioModule } from '@angular/material/radio';  
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TipoMaquinaComponent } from './tipo-maquina/tipo-maquina.component';
import { EncomendasComponent } from './encomendas/encomendas.component';
import { UserComponent } from './user/user.component'; 

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,  
    MatMenuModule,  
    MatDatepickerModule,  
    MatNativeDateModule,  
    MatIconModule,  
    MatRadioModule,  
    MatCardModule,  
    MatSidenavModule,  
    MatFormFieldModule,  
    MatInputModule,  
    MatTooltipModule,  
    MatToolbarModule,
    BrowserAnimationsModule, 
    MatListModule, 
    MatSelectModule, 
    MatOptionModule,
    MatPaginatorModule,

  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    HeroSearchComponent,
    OperacaoComponent,
    HomeComponent,
    LinhaProducaoComponent,
    MaquinasComponent,
    ProdutosComponent,
    PlanosFabricoComponent,
    TipoMaquinaComponent,
    EncomendasComponent,
    UserComponent
  ],

  providers: [HttpClientModule, OperacaoService,MatDatepickerModule,TipoMaquinaService,MaquinaService,LinhaProducaoService,ProdutoService,PlanoFabricoService],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
