import { PlanoFabrico } from './plano-fabrico';

export class Produto {
    produtoId:number;
    produtoNome: string;
    planoFabricoId : number;
    planoFabricoDTO: PlanoFabrico ; 
}
