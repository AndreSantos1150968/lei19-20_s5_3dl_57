import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { TipoMaquina } from './tipo-maquina'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};

@Injectable({
  providedIn: 'root'
})
export class TipoMaquinaService {

  url = 'https://localhost:5001/api/TipoMaquina';  

  constructor(private http: HttpClient) { }

  getAllTiposMaquina(): Observable<TipoMaquina[]> {  
    return this.http.get<TipoMaquina[]>(this.url);  
  }  
  getTipoMaquinaById(TipoMaquinaid: number): Observable<TipoMaquina> {  
    const apiurl = `${this.url}/${TipoMaquinaid}`;
    return this.http.get<TipoMaquina>(apiurl);  
  } 
  createTipoMaquina(TipoMaquina: TipoMaquina): Observable<TipoMaquina> {  
    return this.http.post<TipoMaquina>(this.url, TipoMaquina, httpOptions);  
  }  
  updateTipoMaquina(TipoMaquinaId: number, TipoMaquina: TipoMaquina): Observable<TipoMaquina> {  
    const apiurl = `${this.url}/${TipoMaquinaId}`;
    return this.http.put<TipoMaquina>(apiurl,TipoMaquina, httpOptions);  
  }  
  deleteTipoMaquinaById(TipoMaquinaid: number): Observable<number> {  
    const apiurl = `${this.url}/${TipoMaquinaid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  }  
}
