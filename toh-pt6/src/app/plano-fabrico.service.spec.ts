import { TestBed } from '@angular/core/testing';

import { PlanoFabricoService } from './plano-fabrico.service';

describe('PlanoFabricoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanoFabricoService = TestBed.get(PlanoFabricoService);
    expect(service).toBeTruthy();
  });
});
