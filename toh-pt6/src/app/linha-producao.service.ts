import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { LinhaProducao } from './linha-producao'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};

@Injectable({
  providedIn: 'root'
})
export class LinhaProducaoService {

  url = 'https://localhost:5001/api/LinhaProducao';  

  constructor(private http: HttpClient) { }

  getAllLinhaProducao(): Observable<LinhaProducao[]> {  
    return this.http.get<LinhaProducao[]>(this.url);  
  }  
  getLinhaProducaoById(LinhaProducaoid: number): Observable<LinhaProducao> {  
    const apiurl = `${this.url}/${LinhaProducaoid}`;
    return this.http.get<LinhaProducao>(apiurl);  
  } 
  createLinhaProducao(LinhaProducao: LinhaProducao): Observable<LinhaProducao> {  
    return this.http.post<LinhaProducao>(this.url, LinhaProducao, httpOptions);  
  }  
  updateLinhaProducao(LinhaProducaoId: number, LinhaProducao: LinhaProducao): Observable<LinhaProducao> {  
    const apiurl = `${this.url}/${LinhaProducaoId}`;
    return this.http.put<LinhaProducao>(apiurl,LinhaProducao, httpOptions);  
  }  
  deleteLinhaProducaoById(LinhaProducaoid: number): Observable<number> {  
    const apiurl = `${this.url}/${LinhaProducaoid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  }  
}
