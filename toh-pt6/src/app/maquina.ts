import { TipoMaquina } from './tipo-maquina';

export class Maquina {
    maquinaId:number;
    maquinaNome: string;
    modelo : string;
    tipoMaquinaId: number;
    tipoMaquinaDTO : TipoMaquina;
}
