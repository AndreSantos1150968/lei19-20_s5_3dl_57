import { Operacao } from './operacao'; 

export class PlanoFabrico {
    planoFabricoId : number;
    planoFabricoNome: string;
    listaOperacoes: Operacao[];
}
