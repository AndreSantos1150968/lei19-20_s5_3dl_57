import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  
import { ProdutoService } from '../produto.service';  
import { Produto } from '../produto';
import { PlanoFabricoService } from '../plano-fabrico.service';  
import { PlanoFabrico } from '../plano-fabrico';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {
 
  alunoForm: any;  
  allAlunos: Observable<Produto[]>;  
  alunoIdUpdate = null;
  allOperacoes:PlanoFabrico[]; 
  selectedYears: PlanoFabrico;
  operacoes = new FormControl();

  constructor(private formbulider: FormBuilder, private produtoService: ProdutoService, private planoFabricoService: PlanoFabricoService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      produtoNome: ['', [Validators.required]],
      planoFabricoDTO : [this.selectedYears,[Validators.required]]
    
    });  
    this.loadAllAlunos();
    this.loadAllOperacoes()
  }
  loadAllAlunos() {  
    this.allAlunos = this.produtoService.getAllProduto();  
  } 


  loadAllOperacoes() {  
    this.planoFabricoService.getAllPlanoFabrico().subscribe(res => this.allOperacoes = res);  
  }
  
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    this.alunoForm.reset();  
  } 
  CreateAluno(aluno: Produto) {  
    if (this.alunoIdUpdate == null) {  
      aluno.planoFabricoId = aluno.planoFabricoDTO.planoFabricoId;
      this.produtoService.createProduto(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.produtoId = this.alunoIdUpdate;  
      this.produtoService.updateProduto(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  loadAlunoToEdit(alunoid: number) {  
    this.produtoService.getProdutoById(alunoid).subscribe(aluno=> {  
      this.alunoIdUpdate = aluno.produtoId;  
      this.alunoForm.controls['produtoNome'].setValue(aluno.produtoNome);

    });    
  }  
  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar este Produto ?")) {   
      this.produtoService.deleteProdutoyId(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 
}
