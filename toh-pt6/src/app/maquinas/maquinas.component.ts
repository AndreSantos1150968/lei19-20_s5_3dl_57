import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  
import { MatTableDataSource } from '@angular/material';
import { TipoMaquinaService } from '../tipo-maquina.service';  
import { TipoMaquina } from '../tipo-maquina';
import { MaquinaService } from '../maquina.service';  
import { Maquina } from '../maquina';  

@Component({
  selector: 'app-maquinas',
  templateUrl: './maquinas.component.html',
  styleUrls: ['./maquinas.component.css']
})
export class MaquinasComponent implements OnInit {

  alunoForm: any;  
  allAlunos: Observable<Maquina[]>;  
  alunoIdUpdate = null;
  allTiposMaquina:TipoMaquina[]; 
  selectedTipoMaquina: number;
  tipoMaquina = new FormControl();

  constructor(private formbulider: FormBuilder, private maquinaService: MaquinaService, private tipoMaquinaService: TipoMaquinaService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      'maquinaNome': ['', [Validators.required]],
      'modelo': ['', [Validators.required]],
      'tipoMaquinaDTO' : [null,[Validators.required]],
    });  
    this.loadAllAlunos();
    this.loadAllTipoMaquinas()
  }
  loadAllAlunos() {  
    this.allAlunos = this.maquinaService.getAllMaquinas();  
  } 

  loadAllTipoMaquinas() {  
    this.tipoMaquinaService.getAllTiposMaquina().subscribe(res => this.allTiposMaquina = res);  
  }
  
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    console.log(aluno); 
    this.alunoForm.reset();  
  } 
  CreateAluno(aluno: Maquina) {  
    if (this.alunoIdUpdate == null) {  
      aluno.tipoMaquinaId = aluno.tipoMaquinaDTO.tipoMaquinaId;
      console.log(aluno); 
      this.maquinaService.createMaquina(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.maquinaId = this.alunoIdUpdate;
      
      aluno.tipoMaquinaId=aluno.tipoMaquinaDTO.tipoMaquinaId;  
      console.log(aluno);
      this.maquinaService.updateMaquina(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  loadAlunoToEdit(alunoid: number) {  
    this.maquinaService.getMaquinaById(alunoid).subscribe(aluno=> {  
      this.alunoIdUpdate = aluno.maquinaId;
      this.alunoForm.controls['maquinaNome'].setValue(aluno.maquinaNome);
      this.alunoForm.controls['modelo'].setValue(aluno.modelo);
      this.alunoForm.controls['tipoMaquinaDTO'].setValue(aluno.tipoMaquinaDTO);
    });    
  }  
  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar esta Máquina?")) {   
      this.maquinaService.deleteMaquinaById(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 

}
