import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Produto } from './produto'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  url = 'https://localhost:5001/api/Produto';  

  constructor(private http: HttpClient) { }

  getAllProduto(): Observable<Produto[]> {  
    return this.http.get<Produto[]>(this.url);  
  }  
  getProdutoById(Produtoid: number): Observable<Produto> {  
    const apiurl = `${this.url}/${Produtoid}`;
    return this.http.get<Produto>(apiurl);  
  } 
  createProduto(Produto: Produto): Observable<Produto> {  
    return this.http.post<Produto>(this.url, Produto, httpOptions);  
  }  
  updateProduto(Produtoaid: number, Produto: Produto): Observable<Produto> {  
    const apiurl = `${this.url}/${Produtoaid}`;
    return this.http.put<Produto>(apiurl,Produto, httpOptions);  
  }  
  deleteProdutoyId(Produtoid: number): Observable<number> {  
    const apiurl = `${this.url}/${Produtoid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  } 
}
