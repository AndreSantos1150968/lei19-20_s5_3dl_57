import { Operacao } from './operacao'; 

export class TipoMaquina {
    tipoMaquinaId : number;
    tipoMaquinaNome: string;
    listaOperacoes: Operacao[];
}
