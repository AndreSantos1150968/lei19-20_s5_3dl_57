import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroesComponent }      from './heroes/heroes.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { OperacaoComponent }   from './operacao/operacao.component';
import { HomeComponent } from './home/home.component';
import { LinhaProducaoComponent } from './linha-producao/linha-producao.component';
import { MaquinasComponent } from './maquinas/maquinas.component';
import { ProdutosComponent } from './produtos/produtos.component';
import { PlanosFabricoComponent } from './planos-fabrico/planos-fabrico.component';
import { TipoMaquinaComponent } from './tipo-maquina/tipo-maquina.component';
import { UserComponent} from './user/user.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'heroes', component: HeroesComponent },
  { path: 'operacao', component: OperacaoComponent },
  { path: 'home', component: HomeComponent },
  { path: 'linha-producao', component: LinhaProducaoComponent },
  { path: 'maquinas', component: MaquinasComponent },
  { path: 'produtos', component: ProdutosComponent },
  { path: 'planos-fabrico', component: PlanosFabricoComponent },
  { path: 'tipo-maquina', component: TipoMaquinaComponent },
  { path: 'user', component: UserComponent}

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
