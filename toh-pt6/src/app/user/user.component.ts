import { Component, OnInit } from '@angular/core';
import { User} from '../user';
import {UserService} from '../user.service';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';  

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: User[];

  constructor(private formBuilder: FormBuilder, private UserService: UserService) { }

  ngOnInit() {
    
  }

  getUsers(): void {
    this.UserService.getUsers()
    .subscribe(u => {
    console.log(u); this.users = u});
  }

  createUsers(username : string, nome: string, password : string, role: string) : void{
    this.UserService.createUser({username, nome, password, role} as User)
    .subscribe(() => this.getUsers());
  }

}
