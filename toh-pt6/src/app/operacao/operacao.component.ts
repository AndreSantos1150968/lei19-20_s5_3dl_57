import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';  
import { MatTableDataSource } from '@angular/material';
import { OperacaoService } from '../operacao.service';  
import { Operacao } from '../operacao';  

@Component({
  selector: 'app-operacao',
  templateUrl: './operacao.component.html',
  styleUrls: ['./operacao.component.css']
})
export class OperacaoComponent implements OnInit {
  alunoForm: any;  
  allAlunos: Observable<Operacao[]>;  
  alunoIdUpdate = null;  

  constructor(private formbulider: FormBuilder, private operacaoService: OperacaoService) { }
  
  ngOnInit() {
    this.alunoForm = this.formbulider.group({  
      operacaoNome: ['', [Validators.required]],   
    });  
    this.loadAllAlunos();  
  }
  loadAllAlunos() {  
    this.allAlunos = this.operacaoService.getAllOperacaoes();  
  } 
  onFormSubmit() {  
    const aluno = this.alunoForm.value;

    this.CreateAluno(aluno);  
    console.log(aluno); 
    this.alunoForm.reset();  
  } 
  CreateAluno(aluno: Operacao) {  
    if (this.alunoIdUpdate == null) {  
      this.operacaoService.createOperacao(aluno).subscribe(  
        () => {  
          this.loadAllAlunos();  
          this.alunoIdUpdate = null;  
          this.alunoForm.reset();  
        }  
      );  
    } else {  
      aluno.operacaoId = this.alunoIdUpdate;  
      this.operacaoService.updateOperacao(this.alunoIdUpdate,aluno).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  loadAlunoToEdit(alunoid: number) {  
    this.operacaoService.getOperacaoById(alunoid).subscribe(aluno=> {  
      this.alunoIdUpdate = aluno.operacaoId;  
      this.alunoForm.controls['operacaoNome'].setValue(aluno.operacaoNome);  
    });    
  }  
  deleteAluno(alunoid: number) {  
    if (confirm("Deseja realmente deletar esta Operação ?")) {   
      this.operacaoService.deleteOperacaoById(alunoid).subscribe(() => {  
        this.loadAllAlunos();  
        this.alunoIdUpdate = null;  
        this.alunoForm.reset();  
      });  
    }  
  }  
  resetForm() {  
    this.alunoForm.reset();  
  } 
}
