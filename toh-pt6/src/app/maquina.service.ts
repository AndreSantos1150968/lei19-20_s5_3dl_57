import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Maquina } from './maquina'; 

var httpOptions = {headers: new HttpHeaders({"Content-Type": "application/json"})};

@Injectable({
  providedIn: 'root'
})
export class MaquinaService {

  url = 'https://localhost:5001/api/Maquina';  

  constructor(private http: HttpClient) { }

  getAllMaquinas(): Observable<Maquina[]> {  
    return this.http.get<Maquina[]>(this.url);  
  }  
  getMaquinaById(TipoMaquinaid: number): Observable<Maquina> {  
    const apiurl = `${this.url}/${TipoMaquinaid}`;
    return this.http.get<Maquina>(apiurl);  
  } 
  createMaquina(TipoMaquina: Maquina): Observable<Maquina> {  
    return this.http.post<Maquina>(this.url, TipoMaquina, httpOptions);  
  }  
  updateMaquina(TipoMaquinaId: number, TipoMaquina: Maquina): Observable<Maquina> {  
    const apiurl = `${this.url}/${TipoMaquinaId}`;
    return this.http.put<Maquina>(apiurl,TipoMaquina, httpOptions);  
  }  
  deleteMaquinaById(TipoMaquinaid: number): Observable<number> {  
    const apiurl = `${this.url}/${TipoMaquinaid}`;
    return this.http.delete<number>(apiurl, httpOptions);  
  }  
}
